package com.company.characters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    Character testRanger;
    @BeforeEach
    void setUp() {
        testRanger = new Ranger("Ranger");
    }

    @Test
    void testDealDamage() {
    }

    @Test
    void testOnCreationIsLevelOne() {
        assertEquals(1, testRanger.getLevel(), "Ranger did not get created with level 1");
    }

    @Test
    void testLevelUpCorrectly() {
        testRanger.levelUp();
        assertEquals(2, testRanger.getLevel(), "Ranger did not level up correctly.");
    }

    @Test
    void testBaseAttributes() {
        int[] attributes = {
                testRanger.getTotalStrength(),
                testRanger.getTotalDexterity(),
                testRanger.getTotalIntelligence()
        };
        int[] expectedAttributes = {1, 7, 1};
        assertEquals(Arrays.toString(expectedAttributes), Arrays.toString(attributes));
    }
}