package com.company.characters;

import com.company.enumus.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    Character testMage;

    @BeforeEach
    void setUp() {
        testMage = new Mage("Mage");
    }

    @Test
    void testDealDamage() {
    }

    @Test
    void testOnCreationIsLevelOne() {
        assertEquals(1, testMage.getLevel(), "Mage did not get created with level 1");
    }

    @Test
    void testLevelUpCorrectly() {
        testMage.levelUp();
        assertEquals(2, testMage.getLevel(), "Mage did not level up correctly.");
    }

    @Test
    void testBaseAttributes() {
        int[] attributes = {
                testMage.getTotalStrength(),
                testMage.getTotalDexterity(),
                testMage.getTotalIntelligence()
        };
        int[] expectedAttributes = {1, 1, 8};
        assertEquals(Arrays.toString(expectedAttributes), Arrays.toString(attributes));
    }
}