package com.company.characters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    Character testWarrior;
    @BeforeEach
    void setUp() {
        testWarrior = new Warrior("Warrior");
    }

    @Test
    void testDealDamage() {
    }

    @Test
    void testOnCreationIsLevelOne() {
        assertEquals(1, testWarrior.getLevel(), "Ranger did not get created with level 1");
    }

    @Test
    void testLevelUpCorrectly() {
        testWarrior.levelUp();
        assertEquals(2, testWarrior.getLevel(), "Ranger did not level up correctly.");
    }

    @Test
    void testBaseAttributes() {
        int[] attributes = {
                testWarrior.getTotalStrength(),
                testWarrior.getTotalDexterity(),
                testWarrior.getTotalIntelligence()
        };
        int[] expectedAttributes = {5, 2, 1};
        assertEquals(Arrays.toString(expectedAttributes), Arrays.toString(attributes));
    }
}