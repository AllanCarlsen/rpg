package com.company.characters;

import com.company.enumus.CharacterClass;

public class Warrior extends Character{
    public Warrior(String name) {
        super(name,
                CharacterClass.WARRIOR,
                5,
                2,
                1
        );
    }

    @Override
    public String dealDamage() {
        return null;
    }

    @Override
    public void levelUp() {
        setTotalStrength(getTotalStrength() + 3);
        setTotalDexterity(getTotalDexterity() + 2);
        setTotalIntelligence(getTotalIntelligence() + 1);
        setLevel(getLevel() + 1);
    }
}
