package com.company.characters;

import com.company.enumus.CharacterClass;

public class Mage extends Character {



    public Mage(String name) {
        super(name,
                CharacterClass.MAGE,
                1,
                1,
                8
                );
    }

    @Override
    public String dealDamage() {
        return  "Dealing damage!";
    }

    @Override
    public void levelUp() {
        setTotalStrength(getTotalStrength() + 1);
        setTotalDexterity(getTotalDexterity() + 1);
        setTotalIntelligence(getTotalIntelligence() + 5);
        setLevel(getLevel() + 1);
    }
}
