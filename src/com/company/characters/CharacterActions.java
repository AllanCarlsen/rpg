package com.company.characters;

public interface CharacterActions {
    String dealDamage();
    void levelUp();
}
