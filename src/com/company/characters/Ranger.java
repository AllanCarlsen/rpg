package com.company.characters;

import com.company.enumus.CharacterClass;

public class Ranger extends Character {


    public Ranger(String name) {
        super(name,
                CharacterClass.RANGER,
                1,
                7,
                1
        );
    }


    @Override
    public String dealDamage() {
        return null;
    }

    @Override
    public void levelUp() {
        setTotalStrength(getTotalStrength() + 1);
        setTotalDexterity(getTotalDexterity() + 5);
        setTotalIntelligence(getTotalIntelligence() + 1);
        setLevel(getLevel() + 1);
    }
}
