package com.company.characters;

import com.company.enumus.CharacterClass;
import com.company.enumus.Slot;
import com.company.items.Armor;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.HashMap;

public abstract class Character implements CharacterActions {
    private String name;
    private final CharacterClass charClass;
    private int level = 1;

    private int totalStrength;
    private int totalDexterity;
    private int totalIntelligence;

    private final HashMap<Slot, Item> equipment = new HashMap<>();

    public Character(String name, CharacterClass charClass, int baseStrength, int baseDexterity, int baseIntelligence) {
        this.name = name;
        this.charClass = charClass;
        this.totalStrength = baseStrength;
        this.totalDexterity = baseDexterity;
        this.totalIntelligence = baseIntelligence;

    }

    public int getTotalDPS() {
        int weaponDPS = 1;
        int mainAttribute = 0;
        if(equipment.containsKey(Slot.WEAPON)){
            Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
            weaponDPS = weapon.getDps();
        }
        switch(charClass){
            case MAGE -> mainAttribute = totalIntelligence;
            case RANGER, ROGUE -> mainAttribute = totalDexterity;
            case WARRIOR -> mainAttribute = totalStrength;
        }

        return (weaponDPS * (1 + mainAttribute/100));
    }

    public CharacterClass getCharClass() {
        return charClass;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public void setEquipment(Item item) {
        this.equipment.put(item.getSlot(), item);
        //Updates the character attributes. (I think I now realize the benefit of making attributes an object/class)
        // I think this is getting too tightly coupled..
        if(item instanceof Weapon){
            int weaponDps = ((Weapon) item).getDps();
        } else if(item instanceof Armor) {
            this.totalStrength += ((Armor) item).getStr();
            this.totalDexterity += ((Armor) item).getDex();
            this.totalIntelligence += ((Armor) item).getIntel();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getTotalStrength() {
        return totalStrength;
    }

    protected void setTotalStrength(int totalStrength) {
        this.totalStrength = totalStrength;
    }

    public int getTotalDexterity() {
        return totalDexterity;
    }

    protected void setTotalDexterity(int totalDexterity) {
        this.totalDexterity = totalDexterity;
    }

    public int getTotalIntelligence() {
        return totalIntelligence;
    }

    protected void setTotalIntelligence(int totalIntelligence) {
        this.totalIntelligence = totalIntelligence;
    }

    @Override
    public String toString() {
        return ("Character{" +
                "name='%s', " +
                "charClass=%s, " +
                "level=%d, " +
                "totalStrength=%d, " +
                "totalDexterity=%d, " +
                "totalIntelligence=%d, " +
                "equipment=%s}")
                .formatted(name, charClass, level, totalStrength, totalDexterity, totalIntelligence, equipment);
    }

    public String printStats(){
        return name + "'s stats:" +
                "\n\tLvl.: " + level +
                "\n\tStr.: " + totalStrength +
                "\n\tDex.: " + totalDexterity +
                "\n\tInt.: " + totalIntelligence +
                "\n";
    }
}
