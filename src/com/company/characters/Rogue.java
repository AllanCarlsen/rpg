package com.company.characters;

import com.company.enumus.CharacterClass;

public class Rogue extends Character{
    public Rogue(String name) {
        super(name,
                CharacterClass.ROGUE,
                2,
                6,
                1
        );
    }

    @Override
    public String dealDamage() {
        return null;
    }

    @Override
    public void levelUp() {
        setTotalStrength(getTotalStrength() + 1);
        setTotalDexterity(getTotalDexterity() + 4);
        setTotalIntelligence(getTotalIntelligence() + 1);
        setLevel(getLevel() + 1);
    }
}
