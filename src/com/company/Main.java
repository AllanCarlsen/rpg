package com.company;


import com.company.characters.Character;
import com.company.characters.Mage;
import com.company.characters.Ranger;
import com.company.enumus.ArmorType;
import com.company.enumus.Slot;
import com.company.enumus.WeaponType;
import com.company.items.Armor;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // write your code here
        List<Character> characterArrayList = new ArrayList<>();
        Character mage = new Mage("Allan");
        Character ranger = new Ranger("Frederikke");

        characterArrayList.add(mage);
        characterArrayList.add(ranger);

        Weapon sword = new Weapon(
            "Blade of the Ruined King",
            5,
            Slot.WEAPON,
            10,
            2,
            WeaponType.SWORD
        );
        Armor armor = new Armor(
                "Cloth armor",
                3,
                Slot.BODY,
                ArmorType.CLOTH,
                5,
                10,
                1
        );

        ranger.setEquipment(sword);
        ranger.setEquipment(armor);
        System.out.println(ranger.toString());




        for (Character character: characterArrayList) {
            System.out.println(character.toString());
            character.levelUp();
            System.out.println(character.toString());
        }
        System.out.println(ranger.getTotalDPS());
        for (int i = 0; i <20 ; i++) {
            ranger.levelUp();
        }
        System.out.println(ranger.toString());
        System.out.println(ranger.getTotalDPS());



    }



}
