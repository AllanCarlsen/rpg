package com.company.items;

import com.company.enumus.Slot;
import com.company.enumus.WeaponType;

public class Weapon extends Item{
    int damage;
    int attackSpeed;
    WeaponType type;
    int dps;

    public Weapon(String name, int level, Slot slot, int damage, int attackSpeed, WeaponType type) {
        super(name,level,slot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.type = type;

    }

    public int getDps() {
        dps = damage * attackSpeed;
        return dps;
    }

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    @Override
    public String toString() {
        return "Weapon{" + "damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                ", type=" + type +
                ", dps=" + dps +
                '}';
    }
}
