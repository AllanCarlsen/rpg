package com.company.items;

import com.company.enumus.ArmorType;
import com.company.enumus.Slot;

public class Armor extends Item {
    ArmorType armorType;
    private int str;
    private int dex;
    private int intel;

    public Armor(String name, int level, Slot slot, ArmorType armorType, int strength, int dexterity, int intelligence) {
        super(name, level, slot);
        this.armorType = armorType;
        this.str = strength;
        this.dex = dexterity;
        this.intel = intelligence;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getIntel() {
        return intel;
    }

    public void setIntel(int intel) {
        this.intel = intel;
    }

    @Override
    public String toString() {
        return "Armor{" + "armorType=" + armorType +
                ", str=" + str +
                ", dex=" + dex +
                ", intel=" + intel +
                '}';
    }
}
