package com.company.enumus;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
