package com.company.enumus;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
